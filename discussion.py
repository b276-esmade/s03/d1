# Data Structures
# Functions
# Classes and object instances


# Python has several Structures to store collection or multiple items in a single variable
	# 4 Python Data Structures/Types
		# Lists[], Tuples(), Sets{}, Dictionaries
			# Tuples - ("Math", "Science")
			# Set - {"Math","Science"}


# [SECTION 1] Lists
	# Lists are similar to arrays in JS

# String list
names = ["John", "George", "Ringo"]
programs = ["developer career", "pi-shape", "short courses"]

# Number list
duration = [260,180,20]

# Boolean list
truth_variables = [True,False,True,True]


# A list can contain elements with different data types
sample_list = ["Apple", 3, False,"Potato",4, False]


# Get list size
	# The number of elements can be counted using the len() method

print(len(programs))

var_len = len(sample_list)
print(var_len)

# Accessing values/elements in a list
# Index can be accessed by providing the index number of the element


# Access the first item in the list
print(names[0])


# Access the last item in the list
print(names[-1])


# Access a range of values
		# (start,stopper/range) hanggang index1 lng
print(programs[0:2])
		# (start,stopper/range,incrementation)
print(programs[0:2:3])



# Updating Lists
print(f"Current value: {programs[2]}")

programs[2] = "shortcourses"
print(f"New value: {programs[2]}")










# [SECTION 2] List Manipulation
# List has methods that can be used to manipulate the elements within
# Adding list items - the append() allows to insert items to a list

# PUSH in JS - APPEND()
programs.append("global")
print(programs)


# DELETE - DEL
duration.append(360)
print(duration)

del duration[-1]
print(duration)


# Membership Checking - IN
	# returns true/false

print(20 in duration) #true
print(500 in duration) #false


# Sorting Lists - SORT()
	# ascdending order by default

duration.sort()
print(duration)


# Emptying the list - CLEAR()
test_list = [1,3,5,7,9]
print(test_list)
test_list.clear()
print(test_list) #[]





# [SECTION 3] Dictionaries
	# are used to store data values in key:value pairs.
	# This is similar to objects in JS. A collection which is ordered, changeable an does not allow duplicates
	# To create a dictionary, the curly braces ({} is used

person1 = {
	"name": "Brandon",
	"age" : 28,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL","Django"]
}


# To get the number of key-value pairs on the dictionary, the len() method
print(len(person1))


# Accessing values in dictionary
	# To get item in the dictionary, the key name can be referred using square bracket[]

print(person1["name"])


# The keys() method will return
print(person1.keys())

# The values() method will return a list of all the values in the dictionary
print(person1.values())

# The items() method will return each item in a dictionary as key-value pair in a list

print(person1)
print(person1.items())


# Adding key-value pairs can be done with either a new index key and assigning a value or the update method

	# index key
person1["nationality"] = "Filipino"
print(person1)

	# update method
person1.update({"fav_food": "Sinigang"})
print(person1)


# Deleting entries - can be done using pop() method and the del keyword

person1.pop("fav_food")
print(person1)


del person1["nationality"]
print(person1)




# Clear() method
person2 = {
	"name" : "John",
	"age": 18
}

person2.clear()
print(person2)

# Looping through dictionary

for prop in person1:
	print(f"The value of {prop} is {person1[prop]} ")


# Nested Dictionaries - dictionaries can be nested inside each other

person3 = {
	"name" : "Monika",
	"age": 20,
	"occupation": "poet",
	"isEnrolled": True,
	"subjects": ["Python", "SQL","Django"]
}

# person3["subjects"[0]] //to access Python

class_room = {
	"student1" : person1,
	"student2" : person3
}


print(class_room["student2"]["subjects"][1]) #SQL